#include "usine.hpp"
#include <iostream>

int UsineCarte::compteur = 0;

UsineCarte::UsineCarte(/* args */) {
    compteur = 0;
    cdc = 52;
}

UsineCarte::UsineCarte(int nb) : cdc(nb) {
    compteur = 0;
}

UsineCarte::UsineCarte(const UsineCarte & uc) {

}

int UsineCarte::getCdc() const {
    return cdc;
}

std::unique_ptr<Carte> UsineCarte::getCarte() const {
    std::unique_ptr<Carte> p(new Carte());
    p->setValeur(compteur);
    compteur += 1;
    if (compteur > cdc) {
        p = nullptr;
    }
    return p;
}

UsineCarte & UsineCarte::operator=(const UsineCarte & uc) {
    return *this;  
}

UsineCarte::~UsineCarte() {
}
