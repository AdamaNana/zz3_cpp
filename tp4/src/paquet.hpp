#ifndef PAQUET_HPP
#define PAQUET_HPP

#include <vector>
#include <memory>
#include <sstream>
#include "carte.hpp"
#include "usine.hpp"

//using paquet_t = std::vector<std::unique_ptr<Carte>>;

class Paquet {
    private:
        
    public:
        std::vector<std::unique_ptr<Carte>> vect;
        Paquet(/* args */);
        friend std::stringstream& 
            operator<<(std::stringstream& ss, Paquet & paquet);
        friend std::ostream& 
            operator<<(std::ostream& ss, Paquet & paquet);
        std::unique_ptr<Carte>& operator[](int i);
        ~Paquet();
};

using paquet_t = Paquet;

void remplir(paquet_t &paquet, UsineCarte &usine);

#endif