#ifndef USINECARTE_HPP
#define USINECARTE_HPP
#include <memory>
#include "carte.hpp"

class UsineCarte {
    private:
        UsineCarte(const UsineCarte & uc);
        UsineCarte & operator=(const UsineCarte & c);
        int cdc;
        
    public:
        static int compteur;
        UsineCarte(/* args */);
        UsineCarte(int);
        int getCdc() const;

        std::unique_ptr<Carte> getCarte() const;
        ~UsineCarte();
};

#endif