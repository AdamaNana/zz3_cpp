#include "carte.hpp"

int Carte::compteur = 0;

Carte::Carte() {
    compteur += 1;
}

Carte::Carte(const Carte & v) {

}

/*arte::Carte(unsigned value) {
    valeur = value;
}*/

void Carte::setValeur(unsigned value) {
    valeur = value;
}

unsigned Carte::getValeur() const {
    return valeur;
}

Carte & Carte::operator=(const Carte & c) {
    if (this!=&c) {
        valeur = c.valeur;
    }
    return *this;
}

int Carte::getCompteur() {
    return compteur;
}

Carte::~Carte() {
    compteur -= 1;
}
