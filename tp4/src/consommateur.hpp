#ifndef CONSOMMATEUR_HPP
#define CONSOMMATEUR_HPP

#include <memory>
#include "ressource.hpp"

class Consommateur {
    private:
        int besoin;
        std::shared_ptr<Ressource> ressources;
        
    public:
        Consommateur(/* args */);
        Consommateur(int b, std::shared_ptr<Ressource>& r);
        void puiser();
        ~Consommateur();
};

#endif