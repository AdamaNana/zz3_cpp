#include "ressource.hpp"
#include <stdexcept>
#include <iostream>
Ressource::Ressource(int value) {
    stock = value;
}

int Ressource::getStock() const {
    return stock;
}

void Ressource::consommer(int consommation) {
    stock = stock - consommation < 0 ? 0 : stock - consommation;
}

std::ostream& operator<<(std::ostream& ss, std::vector<std::weak_ptr<Ressource>> & vect) {

    std::vector<std::weak_ptr<Ressource>>::iterator it = vect.begin();
    while (it != vect.end()) {
        std::shared_ptr<Ressource> tmp = (*it).lock();
        if (tmp) {
            if(tmp->getStock() == 0) {
                ss << "-" << " ";
            } else {
                ss << tmp->getStock() << " ";
            }
        } else {
            ss << "-" << " ";
        }
        it++;
    }
    return ss;
}

Ressource::~Ressource() {
}