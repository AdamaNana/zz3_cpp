#include "paquet.hpp"

Paquet::Paquet(/* args */) {
}

void remplir(paquet_t &paquet, UsineCarte &usine) {
    std::unique_ptr<Carte> tmp = usine.getCarte();
    while (tmp != nullptr) {
        (paquet.vect).push_back(std::move(tmp));
        tmp =  usine.getCarte();
    }
}

std::unique_ptr<Carte>& Paquet::operator[](int i) {
    return vect[i];
}

std::stringstream& operator<<(std::stringstream& ss, Paquet & paquet) {
    std::vector<std::unique_ptr<Carte>>::iterator it = (paquet.vect).begin();
    while (it != (paquet.vect).end()) {
        ss << (*it)->getValeur() << " ";
        it++;
    }
    return ss;
}

std::ostream& operator<<(std::ostream& ss, Paquet & paquet) {
    std::vector<std::unique_ptr<Carte>>::iterator it = (paquet.vect).begin();
    while (it != (paquet.vect).end()) {
        ss << (*it)->getValeur() << " ";
        it++;
    }
    return ss;
}

Paquet::~Paquet() {
}