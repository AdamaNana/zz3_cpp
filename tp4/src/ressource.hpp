#ifndef RESSOURCE_HPP
#define RESSOURCE_HPP

#include <vector>
#include <memory>
#include <sstream>

class Ressource {
    private:
        int stock;
    public:
        Ressource(int stock);
        int getStock() const;
        void consommer(int consommation);
        friend std::ostream& 
            operator<<(std::ostream& ss, std::vector<std::weak_ptr<Ressource>> & vect);
        ~Ressource();
};

using ressources_t = typename std::vector<std::weak_ptr<Ressource>>;

#endif