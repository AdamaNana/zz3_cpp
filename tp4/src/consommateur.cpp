#include "consommateur.hpp"

Consommateur::Consommateur(/* args */) {
}

Consommateur::Consommateur(int b, std::shared_ptr<Ressource>& r) : besoin(b), ressources(r) {
    
}

void Consommateur::puiser() {
    if(ressources != nullptr) {
        ressources->consommer(besoin);
        if (ressources->getStock() == 0) {
            ressources = nullptr;
        }
    }
}

Consommateur::~Consommateur() {
}