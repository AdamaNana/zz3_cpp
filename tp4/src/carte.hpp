#ifndef CARTE_HPP
#define CARTE_HPP

class Carte {
    private:
        unsigned valeur;
        Carte(const Carte & v);
        //Carte(unsigned value);
        Carte & operator=(const Carte & c);
        
    public:
        static int compteur;
        Carte();
        void setValeur(unsigned );
        static int getCompteur();
        unsigned getValeur() const;
        ~Carte();
};

#endif