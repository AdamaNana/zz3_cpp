#include "valeur.hpp"

Valeur::Valeur() : valeur(0), nom("inconnu") {
}

Valeur::Valeur(double a, std::string n) : valeur(a), nom(n) {
}

Valeur::Valeur(double value) : valeur(value) {
}

double Valeur::getNombre() const {
    return valeur;
}

double Valeur::getNote() const {
    return valeur;
}

std::string Valeur::getEtudiant() const {
    return nom;
}

void Valeur::setNote(double a) {
    valeur = a;
}

void Valeur::setEtudiant(std::string n) {
    nom = n;
}

void Valeur::setNombre(double value) {
    valeur = value;
}

Valeur::~Valeur() {
}