#include "echantillon.hpp"
#include <algorithm>
#include <stdexcept>

Echantillon::Echantillon(/* args */)
{
}

int Echantillon::getTaille() const {
    return vect.size();
}

void Echantillon::ajouter(Valeur v) {
    vect.push_back(v);
}

Valeur Echantillon::getValeur(int i) const {
    if(int(vect.size()) <= i ) {
        throw std::out_of_range("Out of range for indice");
    } else {
        return vect[i];
    }
}

Valeur Echantillon::getMinimum() {
    if(vect.size() == 0) {
        throw std::domain_error("L'échantillon est vide");
    } else {
        std::vector<Valeur>::iterator result = std::min_element(vect.begin(), vect.end(), [](Valeur a, Valeur b) { return a.getNombre() < b.getNombre();} );
        return *result;
    }
}

Valeur Echantillon::getMaximum() {
    if(vect.size() == 0) {
        throw std::domain_error("L'échantillon est vide");
    } else {
        std::vector<Valeur>::iterator result = std::max_element(vect.begin(), vect.end(), [](Valeur a, Valeur b) { return a.getNombre() < b.getNombre();});
        return *result;
    }
}

Echantillon::~Echantillon()
{
}
