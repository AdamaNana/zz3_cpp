#ifndef ECHANTILLON_HPP
#define ECHANTILLON_HPP

#include "valeur.hpp"
#include <vector>

class Echantillon
{
private:
    std::vector<Valeur> vect;
public:
    Echantillon(/* args */);
    int getTaille() const;
    void ajouter(Valeur v);
    Valeur getValeur(int i) const;
    Valeur getMinimum();
    Valeur getMaximum();
    ~Echantillon();
};

#endif