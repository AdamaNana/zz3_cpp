#include "classe.hpp"

Classe::Classe(double a, double b) : BorneInf(a), BorneSup(b), Quantite(0) {
}

Classe::Classe(const Classe& c) : BorneInf(c.getBorneInf()), BorneSup(c.getBorneSup()), Quantite(c.getQuantite()) {

}

double Classe::getBorneInf() const {
    return BorneInf;
}

double Classe::getQuantite() const {
    return Quantite;
}

double Classe::getBorneSup() const {
    return BorneSup;
}

void Classe::setBorneInf(double a) {
    BorneInf = a;
}

void Classe::setQuantite(int q) {
    Quantite = q;
}

void Classe::setBorneSup(double b) {
   BorneSup = b;
}

void Classe::ajouter() {
    Quantite += 1;
}

bool Classe::operator<(const Classe &c) const {
    return BorneInf < c.getBorneInf();
}

bool Classe::operator>(const Classe &c) const {
    return BorneInf > c.getBorneInf();
}

Classe::~Classe()
{
}
