#ifndef HISTOGRAMME_HPP
#define HISTOGRAMME_HPP

#include <vector>
#include "Classe.hpp"
#include "echantillon.hpp"
#include <set>
#include <map>

/*class Histogramme {
    private:
        std::set<Classe> hist;
    public:
        Histogramme(double inf, double sup, int nbclasses);
        std::set<Classe> getClasses() const;
        void ajouter(Echantillon e);
        ~Histogramme();
};*/

template <typename FONCT = std::less<Classe>>
class Histogramme {
    private:
        std::set<Classe, FONCT> hist;
        std::multimap<Classe, Valeur> hist_map;
    public:
        Histogramme(double inf, double sup, int nbclasses);
        template <typename T>
        Histogramme(const Histogramme<T> &h);
        std::set<Classe, FONCT> getClasses() const;
        std::multimap<Classe, Valeur> getValeurs() const;
        auto getValeurs(const Classe& c) const;
        void ajouter(Echantillon e);
        void ajouter(Valeur v);
        ~Histogramme();
};


template <typename FONCT>
template <typename T>
Histogramme<FONCT>::Histogramme(const Histogramme<T> &h) {
    for (const Classe & c : h.getClasses()) {
        hist.insert(c);
    }
}


template <typename FONCT>
Histogramme<FONCT>::Histogramme(double inf, double sup, int nbclasses) {
    int amplitude = (sup - inf) / nbclasses;
    double inf_tmp = inf;
    double sup_tmp = inf + amplitude;
    for (int i=0; i<nbclasses; i++) {
        hist.insert(Classe(inf_tmp, sup_tmp));
        inf_tmp = sup_tmp;
        sup_tmp = inf_tmp + amplitude;
    }
}

template <typename FONCT>
void Histogramme<FONCT>::ajouter(Echantillon e) {
    for (int i=0; i<e.getTaille(); i++) {
        double value = e.getValeur(i).getNombre();
        auto place = std::find_if(hist.begin(), hist.end(), [value](Classe c) { return (value < c.getBorneSup() && value >= c.getBorneInf());} );
        Classe c(*place);
        c.ajouter();
        double tmp = (*place).getNote() + value;
        hist.erase(place);
        hist.insert(c);
    }
}

template <typename FONCT>
std::set<Classe, FONCT> Histogramme<FONCT>::getClasses() const {
    return hist;
}

template <typename FONCT>
std::multimap<Classe, Valeur> Histogramme<FONCT>::getValeurs() const {
    return hist_map;
}

template <typename FONCT>
auto Histogramme<FONCT>::getValeurs(const Classe& c) const {
    return std::equal_range(hist_map.begin(), hist_map.end(), c);
}

bool operator< (const std::pair<Classe, Valeur> & p, const Classe& c) {
    return p.first.getBorneInf() < c.getBorneInf();
}

bool operator< (const Classe& c, const std::pair<Classe, Valeur> & p) {
    return p.first.getBorneInf() > c.getBorneInf();
}

template <typename FONCT>
void Histogramme<FONCT>::ajouter(Valeur value) {
    auto place = std::find_if(hist.begin(), hist.end(), [value](Classe c) { return (value.getNombre() < c.getBorneSup() && value.getNombre()  >= c.getBorneInf());} );
    Classe c(*place);
    c.ajouter();
    hist.erase(place);
    hist.insert(c);
    hist_map.insert(std::pair<Classe, Valeur>{c, value});
}

template <typename FONCT>
Histogramme<FONCT>::~Histogramme() {
}

#endif