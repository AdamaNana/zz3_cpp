#ifndef VALEUR_HPP
#define VALEUR_HPP

#include <string>

class Valeur {
    private :
        double valeur;
        std::string nom;
    
    public :
        Valeur();
        Valeur(double a, std::string n);
        Valeur(double value);
        double getNombre() const;
        double getNote() const;
        std::string getEtudiant() const;
        void setNote(double a);
        void setEtudiant(std::string n);
        void setNombre(double value);
        ~Valeur();
};

#endif


