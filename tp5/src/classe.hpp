#ifndef CLASSE_HPP
#define CLASSE_HPP

class Classe {
    private:
        double BorneInf;
        double BorneSup;
        int Quantite;
    public:
        Classe(double a, double b);
        Classe(const Classe& c);
        double getBorneInf() const;
        double getBorneSup() const;
        double getQuantite() const;
        void setBorneInf(double );
        void setQuantite(int );
        void setBorneSup(double );
        void ajouter();
        bool operator<(const Classe &c) const;
        bool operator>(const Classe &c) const;
        ~Classe();
};

template <typename T>
class ComparateurQuantite {
    public :
        bool operator() (const Classe& c1, const Classe& c2) {
            bool r = c1.getQuantite() > c2.getQuantite();
            if(c1.getQuantite() == c2.getQuantite()) {
                r = c1.getBorneInf() < c2.getBorneInf();
            }
            return r;
        } 
};

#endif