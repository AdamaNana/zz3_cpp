#ifndef FACTORIELLE_HPP
#define FACTORIELLE_HPP

template<int N>
class Factorielle {
    public :
        static unsigned long valeur;

};

template<> 
unsigned long Factorielle<0>::valeur = 1; 

template<int N>
unsigned long Factorielle<N>::valeur = N * Factorielle<N-1>::valeur;

#endif