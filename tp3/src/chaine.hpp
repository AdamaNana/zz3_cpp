#ifndef CHAINE_HPP
#define CHAINE_HPP

#include "exception.hpp"
#include <sstream>

template <typename... ARGS>
std::string chaine(std::tuple<ARGS...> &t);

template <typename T, size_t... Is>
std::string chaine(T &t, std::index_sequence<Is...>);

template <typename T, typename... ARGS> 
std::string chaine(T x, ARGS... args);


template <typename T> 
std::string chaine(T &x) {
    throw ExceptionChaine(x);
}

std::string chaine(std::string &x) {
    return x;
}

std::string chaine(int &x) {
    return std::to_string(x);
}

//Pourquoi to_string rajoute quatres 0 à la fin ??
std::string chaine(double &x) {
    return std::to_string(x);
}

template <typename T, typename... ARGS> 
std::string chaine(T x, ARGS... args) {
    return (chaine(x) + " " + chaine(args...));
}

template <typename T, size_t... Is>
std::string chaine(T &t, std::index_sequence<Is...>) { 
    return chaine(std::get<Is>(t)...);
}

template <typename... ARGS>
std::string chaine(std::tuple<ARGS...> &t){
    return chaine(t, std::make_index_sequence<sizeof...(ARGS)>());
}

#endif