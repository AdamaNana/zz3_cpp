#ifndef EXCEPTION_HPP 
#define EXCEPTION_HPP

#include <exception>
#include <string>

#include "demangle.hpp"

class ExceptionChaine : public std::exception {
    private:
        std::string what_str;

    public:
        template <typename T>
        ExceptionChaine(T x) : what_str("Conversion en chaine impossible pour '" + demangle(typeid(x).name()) + "'") {
        }
        const char * what() const noexcept override {
            return what_str.c_str();
        }
        ~ExceptionChaine() {
        }
};

#endif
