#ifndef SINUS_HPP
#define SINUS_HPP

#include <math.h>

template<int N>
class Sinus {
    public :
        static double valeur(double);
};

template<int N>
double Sinus<N>::valeur(double value) {
    return std::sin(value);
}

#endif