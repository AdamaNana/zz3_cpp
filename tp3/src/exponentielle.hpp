#ifndef EXPONENTIELLE_HPP
#define EXPONENTIELLE_HPP

template<int N>
class Exponentielle {
    public :
        static double valeur(double);

};

template<int N>
double Exponentielle<N>::valeur(double value) {
    return exp(value);
}

#endif