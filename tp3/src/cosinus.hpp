#ifndef COSINUS_HPP
#define COSINUS_HPP

#include <math.h>

template<int N>
class Cosinus {
    public :
        static double valeur(double);
};

template<int N>
double Cosinus<N>::valeur(double value) {
    return std::cos(value);
}

#endif