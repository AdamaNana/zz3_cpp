#ifndef PUISSANCE_HPP
#define PUISSANCE_HPP

#include <math.h>

template<int N>
class Puissance {
    public :
        static double valeur(double);
};

template<int N>
double Puissance<N>::valeur(double value) { 
    return pow(value, N);
}
#endif