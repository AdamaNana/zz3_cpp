#include "Nuage.hpp"
#include <iostream>

Nuage::Nuage() {

}

int Nuage::size() const {
    return nuage.size();
}

void Nuage::ajouter(Point & p) {
    nuage.push_back(&p);
}

Cartesien barycentre(Nuage nuage) {
    Nuage::const_iterator it = nuage.begin();
    Cartesien tmp(0, 0);
    Cartesien c(0, 0);
    int s=0;
    while (it!=nuage.end()) {
        (*it)->convertir(tmp);
        c.setX(c.getX()+tmp.getX());
        c.setY(c.getY()+tmp.getY());
        s+=1;
        it++;     
    }
    c.setX(c.getX()/s);
    c.setY(c.getY()/s);
    return c;
}

Nuage::~Nuage() {
}