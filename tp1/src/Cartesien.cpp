#include "Cartesien.hpp"

Cartesien::Cartesien():Point(0, 0) {
}

Cartesien::Cartesien(double xx, double yy):Point(xx, yy) {

}

Cartesien::Cartesien(Polaire &p) {
    double rayon = p.getDistance();
    double theta = p.getAngle();
    x = rayon * std::cos(theta*M_PI/180);
    y = rayon * std::sin(theta*M_PI/180);
}

double Cartesien::getX() const {
    return x;
}

double Cartesien::getY() const {
    return y;
}

void Cartesien::setX(double xx) {
    x = xx;
}

void Cartesien::setY(double yy) {
    y = yy;
}

void Cartesien::afficher(std::stringstream & flux) const {
    flux << "(x=" << x << ";y=" << y << ")";
}

std::stringstream& operator<<(std::stringstream & sortie, const Cartesien & c) {
    c.afficher(sortie);
    return sortie;
}

void Cartesien::convertir(Polaire & p) const {
    p.setDistance(sqrt(x*x + y*y));
    double theta = 0;
    if (x>0) {
        if (y>=0) {
            theta = std::atan(y/x);
        } else {
            theta = std::atan(y/x) + 2*M_PI;
        }

    } else if (x<0) {
        theta = std::atan(y/x) + M_PI;
    } else if (x==0) {
        if (y>0) {
            theta = M_PI/2;
        } else if (y<0) {
            theta = 3*M_PI/2;
        }
    }
    p.setAngle(theta * 180/M_PI);
}

void Cartesien::convertir(Cartesien & c) const {
    c.setX(x);
    c.setY(y);
}

Cartesien::~Cartesien() {
}