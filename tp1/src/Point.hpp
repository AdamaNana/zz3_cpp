#ifndef POINT_HPP
#define POINT_HPP
#include <sstream>

class Cartesien;
class Polaire;

class Point {
    protected :
        double x;
        double y;

    public :
        Point();
        Point(double, double);
        virtual void afficher(std::stringstream &) const =0; 
        friend std::stringstream& 
            operator<<(std::stringstream& sortie, const Point & p);
        virtual void convertir(Cartesien & c) const = 0;
        virtual void convertir(Polaire & p) const = 0;
        virtual ~Point();
};

#endif