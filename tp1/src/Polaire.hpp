#ifndef POLAIRE_HPP
#define POLAIRE_HPP

#define _USE_MATH_DEFINES

#include "Point.hpp"
#include <cmath>
#include "Cartesien.hpp"
class Cartesien;
class Polaire : public Point {
    private :
        double rayon;
        double theta; 

    public :
        Polaire(); 
        Polaire(double r, double t);
        Polaire(const Cartesien &);
        void afficher(std::stringstream & flux) const;
        double getAngle() const;
        double getDistance() const;
        void setAngle(const double);
        void setDistance(const double);
        friend std::stringstream& 
            operator<<(std::stringstream& sortie, const Polaire & p);
        void convertir(Cartesien & c) const;
        void convertir(Polaire & c) const;
        ~Polaire();
};

#endif