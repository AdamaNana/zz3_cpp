#ifndef NUAGE_HPP
#define NUAGE_HPP

#define _USE_MATH_DEFINES

#include <sstream>
#include <vector>
#include <cmath>
#include "Point.hpp"
#include "Polaire.hpp"
#include "Cartesien.hpp"

class Nuage {
    private :
        std::vector<Point*> nuage;
    public :
        Nuage();
        int size() const;
        void ajouter(Point &);

        typedef std::vector<Point*>::const_iterator const_iterator;

        const_iterator begin() {
            return nuage.begin();
        };
        const_iterator end() {
            return nuage.end();
        };
        Cartesien BarycentreCartesien();
        Polaire BarycentrePolaire();
        ~Nuage();
};

Cartesien barycentre(Nuage);

class BarycentreCartesien {
    public :
        Cartesien operator()(Nuage nuage) {
            return barycentre(nuage);
        }
};

class BarycentrePolaire {
    public :
        Polaire operator()(Nuage nuage) {
            return Polaire(barycentre(nuage));
        }
};

#endif