#include <cstdlib>
#include <vector>

#include "Point.hpp"
#include "Cartesien.hpp"
#include "Polaire.hpp"

int main() {

    Polaire p1(10, 12);
    Polaire p2(5, 6);
    Polaire p3(2, 3);

    Cartesien c1(0, 0);
    Cartesien c2(5, 5);
    Cartesien c3(10, 10);

    return EXIT_SUCCESS;
}