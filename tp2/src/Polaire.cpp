#include <iostream>
#include <sstream>
#include "Polaire.hpp"

Polaire::Polaire():rayon(0), theta(0) {
}

Polaire::Polaire(double r, double t):rayon(r), theta(t) {
}

Polaire::Polaire(const Cartesien &c) {
    double xx = c.getX();
    double yy = c.getY();
    rayon = sqrt(xx*xx + yy*yy);
    theta = std::atan2(yy, xx) * 180 / M_PI;
}

void Polaire::afficher(std::stringstream & flux) const {
    flux << "(a=" << rayon << ";d=" << theta << ")";
}

double Polaire::getAngle() const {
    return theta;
}
double Polaire::getDistance() const {
    return rayon;
}

void Polaire::setAngle(const double angle) {
    theta = angle;
}

void Polaire::setDistance(const double r) {
    rayon = r;
}

std::stringstream& operator<<(std::stringstream & sortie, const Polaire & p) {
    p.afficher(sortie);
    return sortie;
}

void Polaire::convertir(Cartesien & c) const {
    c.setX(rayon * std::cos(theta*M_PI/180));
    c.setY(rayon * std::sin(theta*M_PI/180));
}

void Polaire::convertir(Polaire & p) const {
    p.setAngle(theta);
    p.setDistance(rayon);
}

Polaire::~Polaire() {
}