#ifndef CARTESIEN_HPP
#define CARTESIEN_HPP

#define _USE_MATH_DEFINES

#include "Point.hpp"
#include <sstream>
#include <cmath>
#include "Polaire.hpp"

class Polaire;

class Cartesien : public Point {
    private :

    public :
        Cartesien();
        Cartesien(double, double);
        Cartesien(Polaire &);
        double getX() const;
        double getY() const;
        void setX(double);
        void setY(double);
        void afficher(std::stringstream &) const;
        friend std::stringstream&
            operator<<(std::stringstream & sortie, const Cartesien & c);
        void convertir(Polaire & p) const;
        void convertir(Cartesien & c) const;
        ~Cartesien();
};

#endif