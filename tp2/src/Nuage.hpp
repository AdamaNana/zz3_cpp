#ifndef NUAGE_HPP
#define NUAGE_HPP

#define _USE_MATH_DEFINES

#include <sstream>
#include <vector>
#include <cmath>
#include "Point.hpp"
#include "Polaire.hpp"
#include "Cartesien.hpp"
#include <iostream>

template<typename T>
class Nuage {
    private :
        std::vector<T> nuage;
    public :
        using const_iterator = typename std::vector<T>::const_iterator;
        Nuage();
        const_iterator begin();
        const_iterator end();
        int size() const;
        void ajouter(T);
        ~Nuage();
};

template <typename T>
Nuage<T>::Nuage() {
}

template <typename T>
typename Nuage<T>::const_iterator Nuage<T>::begin() {
    return nuage.begin();
}

template <typename T>
typename Nuage<T>::const_iterator Nuage<T>::end() {
    return nuage.end();
}

template <typename T>
int Nuage<T>::size() const {
    return nuage.size();
}

template <typename T>
void Nuage<T>::ajouter(T p) {
    nuage.push_back(p);
}

template <typename T>
T barycentre_v1(Nuage<T> nuage) {
    Cartesien c(0,0);
    if(nuage.size() != 0 ) {
        typename Nuage<T>::const_iterator it = nuage.begin();
        Cartesien tmp(0, 0);
        int s=0;
        while (it!=nuage.end()) {
            (*it).convertir(tmp);
            c.setX(c.getX()+tmp.getX());
            c.setY(c.getY()+tmp.getY());
            s+=1;
            it++;     
        }
        c.setX(c.getX()/s);
        c.setY(c.getY()/s);
    }
    return T(c);
}

Polaire barycentre_v1(Nuage<Polaire> nuage) {
    Polaire c(0,0);
    if(nuage.size() != 0 ) {
        typename Nuage<Polaire>::const_iterator it = nuage.begin();
        Polaire tmp(0, 0);
        int s=0;
        while (it!=nuage.end()) {
            (*it).convertir(tmp);
            c.setAngle(c.getAngle() + tmp.getAngle());
            c.setDistance(c.getDistance() + tmp.getDistance());
            s+=1;
            it++;     
        }
        c.setAngle(c.getAngle()/s);
        c.setDistance(c.getDistance()/s);
    }
    return Polaire(c);
}

template <typename T>
Cartesien barycentre_v2(T nuage) {
    Cartesien c(0,0);
    if(nuage.size() != 0 ) {
        typename T::const_iterator it = nuage.begin();
        Cartesien tmp(0, 0);
        int s=0;
        while (it!=nuage.end()) {
            (*it).convertir(tmp);
            c.setX(c.getX() + tmp.getX());
            c.setY(c.getY() + tmp.getY());
            s+=1;
            it++;     
        }
        c.setX(c.getX()/s);
        c.setY(c.getY()/s);
    }
    return c;
}

template <typename T>
Nuage<T>::~Nuage() {
}

#endif