#include "Point.hpp"
#include "Cartesien.hpp"
#include "Polaire.hpp"

Point::Point(): x(0), y(0) {
}

Point::Point(double xx, double yy): x(xx), y(yy) {
}


std::stringstream& operator<<(std::stringstream & sortie, const Point & p) {
    p.afficher(sortie);
    return sortie;
}

Point::~Point() {

}